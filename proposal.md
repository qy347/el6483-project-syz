Project Proposal for EL6483
==========================

## Team

Team members (up to four). Write name and email address of each team member:


1. Liang Shan  ls3818@nyu.edu

2. Qianyu Yin  qy347@nyu.edu

3. Ziyue Zhang zz1034@nyu.edu

4.

## Idea

Describe your project idea, in 1-2 paragraphs. Also explain your motivation for this project idea - why is this project interesting to you?

```
Our object is to develop a touch screen application and build a drawing board. 
Today more and more electronic devices are equipped with touch screen, such as phones and computers. 
So we want to study how the touch screen is driven through the project. 
If time allows, we will add more functions such as choosing colors.

```

## Materials

The STM32F4 Discovery board should be the primary microcontroller in your project. However, you may use other devices (e.g. a secondary microcontroller or external peripherals) if you need to (based on your project requirements).

If you plan to use any extra hardware, list each device and explain what it will be used for. If you need to buy a device, include a link to the webpage for the product you intend to purchase. If you already own a device, link to the webpage from which you purchased it.

```

This is LCD screen.
http://www.adafruit.com/products/1770

```

## Milestones

Milestones mark specific points along a project timeline. Your project should have two milestones, and you should plan to demonstrate a working prototype at each:

* 16 April 2015: At this stage, you should have a preliminary working prototype of *something*. It may not have all the functionality of your final project, but you should have something operational to show at this point. Describe what you will demonstrate during this week.

```
Light up the screen and show the X,Y coordinates of a touch in openocd.
```

* 7 May 2015: During finals week, you will demonstrate your completed project. What do you intend to demonstrate at this time? What features will your final project have?
```
Finish the project. On the screen it can show the track of the touch points.
```

## Plan of work

 * Describe (in detail) how you plan to divide the work for your project among the team members. This is a group project, and you are all responsible for ensuring the project's successful outcome; but I want to see how you plan to divide the work amongst yourselves. Who is going to be responsible for each part of the project? Make sure to divide the work in a reasonable way.


 * You will have roughly 6 weeks to work on this project. Describe what each team member needs to accomplish in each week, and what the group as a whole needs to accomplish in each week. (Plan ahead; for example, if you need to purchase extra materials, you may need to wait a couple of weeks for those to arrive.) Indicate how many hours of work is expected from each team member in each week - be realistic.

```
Week 1: Purchase LCD screen. Together, we study the features and theory of touch screen. Study the driver library, study how to configure the related registers, learn and decide the communication protocol. wire up the touch screen and light up (8 hours each member)

Week 2: Liang shan configures the rejesters; Qianyu Yin Lists the main functions of the programm and Ziyue Zhang determines the control flow. Together we form a preliminary program.(8 hours each member)

Week 3: Liang Shan and Qianyu Yin build the hardware platform and write and complie each functions in the program separately.Ziyue Zhang show the X,Y coordinates of a touch in openocd(10 hours each member)

Week 4: Qianyu Yin records the touch position, Ziyue Zhang writes the functions to track the trace and Liang Shan lights up the touched pixels.(10 hours each member)

Week 5,6: Bug fix,try some optional features and prepare the presentation. (10 hours each member)

```







