#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "stm32f4xx.h"
#include "stm32f4xx_adc.h"
#include "tm_stm32f4_adc.h"
#include "stm32f4xx_dma.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_exti.h"
#include "misc.h"
#include "stm32f4xx_syscfg.h"
#include "stm32f4xx_spi.h"
#include "defines.h"
#include "tm_stm32f4_ili9341.h"
#include "tm_stm32f4_fonts.h"

/******************************************************************************/

volatile uint16_t ADCConvertedValues[4];
volatile uint16_t X1,Y1,X2,Y2;              // ADC reading
volatile uint16_t X,Y;                      // Averaged ADC reading
uint16_t x, y;                              // ILI9341 coordinate x,y
uint32_t color;                             // Drawing color

void Default_Screen();
void Choose_Color(uint16_t X,uint16_t Y);
void Screen_Reset();
void Coordinate_Caculation(uint16_t X,uint16_t Y);
void Print(uint16_t x,uint16_t y);
void Delay(uint32_t);

/******************************************************************************/

void GPIO_Configuration(void)
{
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);

  // important: reset Y-
  GPIO_DeInit(GPIOB);

  GPIO_InitTypeDef GPIO_InitStructure;
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5;           // define Y+   A5
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;        // Y+ analog mode
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOA, &GPIO_InitStructure);              // Y+ to read X coordinate

  GPIO_InitTypeDef GPIO_InitStructure4;               // define X+   C0
  GPIO_InitStructure4.GPIO_Pin = GPIO_Pin_0;
  GPIO_InitStructure4.GPIO_Mode = GPIO_Mode_OUT;      // X- set to output mode
  GPIO_Init(GPIOC, &GPIO_InitStructure4);

  GPIO_InitTypeDef GPIO_InitStructure5;
  GPIO_InitStructure5.GPIO_Pin = GPIO_Pin_5;          // define X-   D5
  GPIO_InitStructure5.GPIO_Mode = GPIO_Mode_OUT;      // X- set to output mode
  GPIO_Init(GPIOD, &GPIO_InitStructure5);

  GPIO_SetBits(GPIOC, GPIO_Pin_0);                    // set X+ to high
  GPIO_ResetBits(GPIOD, GPIO_Pin_5);                  // set X- to low
}

void GPIO_Configuration2(void){
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);

  // important: reset X-
  GPIO_DeInit(GPIOD);

  GPIO_InitTypeDef GPIO_InitStructure1;
  GPIO_InitStructure1.GPIO_Pin = GPIO_Pin_0;          // define X+   C0
  GPIO_InitStructure1.GPIO_Mode = GPIO_Mode_AN;       // Analog input to read Y coordinate
  GPIO_InitStructure1.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOC, &GPIO_InitStructure1);

  GPIO_InitTypeDef GPIO_InitStructure2;
  GPIO_InitStructure2.GPIO_Pin = GPIO_Pin_5;          // define Y+   A5
  GPIO_InitStructure2.GPIO_Mode = GPIO_Mode_OUT;      // Y+ set to output mode
  GPIO_Init(GPIOA, &GPIO_InitStructure2);

  GPIO_InitTypeDef GPIO_InitStructure3;
  GPIO_InitStructure3.GPIO_Pin = GPIO_Pin_2;          // define Y-   B2
  GPIO_InitStructure3.GPIO_Mode = GPIO_Mode_OUT;      // Y+ set to output mode
  GPIO_Init(GPIOB, &GPIO_InitStructure3);

  GPIO_SetBits(GPIOA, GPIO_Pin_5);                    // set Y+ to high
  GPIO_ResetBits(GPIOB, GPIO_Pin_2);                  // set Y- to low
}

/******************************************************************************/

void ADC1_Configuration(void){
  ADC_InitTypeDef       ADC_InitStructure;
  ADC_CommonInitTypeDef ADC_CommonInitStructure;
  DMA_InitTypeDef       DMA_InitStructure;

  // Enable peripheral clocks
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA2, ENABLE);
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);

  // DMA2_Stream0 channel0 configuration

  // ADC are set to DMA2 Sream0 Channel0, according to official manual
  DMA_DeInit(DMA2_Stream0);
  DMA_InitStructure.DMA_Channel = DMA_Channel_0;
  DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)&ADC1->DR;
  DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t)&ADCConvertedValues[0];
  DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralToMemory;
  DMA_InitStructure.DMA_BufferSize = 4;
  DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
  DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
  DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
  DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
  DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
  DMA_InitStructure.DMA_Priority = DMA_Priority_High;
  DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
  DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_HalfFull;
  DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
  DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
  DMA_Init(DMA2_Stream0, &DMA_InitStructure);
  // DMA2_Stream0 enable
  DMA_Cmd(DMA2_Stream0, ENABLE);

  //ADC Common Init
  ADC_CommonInitStructure.ADC_Mode = ADC_Mode_Independent;
  ADC_CommonInitStructure.ADC_Prescaler = ADC_Prescaler_Div2;
  ADC_CommonInitStructure.ADC_DMAAccessMode = ADC_DMAAccessMode_Disabled;
  ADC_CommonInitStructure.ADC_TwoSamplingDelay = ADC_TwoSamplingDelay_5Cycles;
  ADC_CommonInit(&ADC_CommonInitStructure);

  // ADC1 Init
  ADC_InitStructure.ADC_Resolution = ADC_Resolution_8b;
  ADC_InitStructure.ADC_ScanConvMode = ENABLE;
  ADC_InitStructure.ADC_ContinuousConvMode = ENABLE;
  ADC_InitStructure.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None;
  ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_T1_CC1;
  ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
  ADC_InitStructure.ADC_NbrOfConversion = 2;
  ADC_Init(ADC1, &ADC_InitStructure);

  // ADC1 regular channel configuration
  ADC_RegularChannelConfig(ADC1, ADC_Channel_5, 1, ADC_SampleTime_3Cycles); // PA5
  ADC_RegularChannelConfig(ADC1, ADC_Channel_10, 2, ADC_SampleTime_3Cycles); // PC0

  // Enable DMA request after last transfer (Single-ADC mode)
  ADC_DMARequestAfterLastTransferCmd(ADC1, ENABLE);

  // Enable ADC1 DMA
  ADC_DMACmd(ADC1, ENABLE);

  // Enable ADC1
  ADC_Cmd(ADC1, ENABLE);

  // Start ADC1 Software Conversion
  ADC_SoftwareStartConv(ADC1);
}

/******************************************************************************/

// Use delay to increase the precison
void Delay(uint32_t i){
  for (; i!= 0; i--);
}

// Set default background for drawing board UI
void Default_Screen(){
  TM_ILI9341_Rotate(TM_ILI9341_Orientation_Portrait_2);
  TM_ILI9341_Fill(ILI9341_COLOR_BLACK);
  color=ILI9341_COLOR_WHITE;
  TM_ILI9341_DrawFilledRectangle(0, 0, 40, 60, ILI9341_COLOR_BLUE2);
  TM_ILI9341_DrawFilledRectangle(0, 60, 40, 120, ILI9341_COLOR_RED);
  TM_ILI9341_DrawFilledRectangle(0, 120, 40, 180, ILI9341_COLOR_WHITE);
  TM_ILI9341_DrawFilledRectangle(0, 180, 40, 240, ILI9341_COLOR_YELLOW);
  TM_ILI9341_DrawFilledRectangle(0, 240, 40, 320, ILI9341_COLOR_GRAY);
}

// Setting touching area for selecting colors
void Choose_Color(uint16_t x,uint16_t y){
  if(x<=40 && y<=60) color=ILI9341_COLOR_BLUE2;
  else if (x<=40 && y>=61 && y<=120) color=ILI9341_COLOR_RED;
  else if (x<=40 && y>=121 && y<=180) color=ILI9341_COLOR_WHITE;
  else if (x<=40 && y>=181 && y<=240) color=ILI9341_COLOR_YELLOW;
}

// Set reset background for drawing board UI
void Screen_Reset(){
  TM_ILI9341_Fill(ILI9341_COLOR_BLACK);
  TM_ILI9341_DrawFilledRectangle(0, 0, 40, 60, ILI9341_COLOR_BLUE2);
  TM_ILI9341_DrawFilledRectangle(0, 60, 40, 120, ILI9341_COLOR_RED);
  TM_ILI9341_DrawFilledRectangle(0, 120, 40, 180, ILI9341_COLOR_WHITE);
  TM_ILI9341_DrawFilledRectangle(0, 180, 40, 240, ILI9341_COLOR_YELLOW);
  TM_ILI9341_DrawFilledRectangle(0, 240, 40, 320, ILI9341_COLOR_GRAY);
}

// Transform X and Y to the ILI9341 coordinate x,y
// And also add offset to increase the precision
void Coordinate_Caculation(uint16_t X,uint16_t Y){
  X-=16;
  Y-=60;
  if(Y>=0 && Y<=40) x=(X-10)*2.00+(70-(Y/2))*(0.5+(X/240));
  else if(Y>=150 && Y<=180) x=(X-10)*2.00+(80-(Y/2))*(0.6+(X/240));
  else x=(X-10)*2.00+(80-(Y/2))*(0.37+(X/240));

  y=(Y-5)*1.88;
}

void Print(uint16_t x,uint16_t y){
  // When touch detected in the color blocks, call Choose_Color function to
  // change drawing color, otherwise, light up the pixels arouch the touch point.
  if(x<=40 && y<=240) Choose_Color(x,y);

  // If touch is detected in the right bottom block, reset screen
  else if(x<=40 && y>=241) Screen_Reset();

  else TM_ILI9341_DrawFilledCircle(x, y, 3, color);
}



void main(void){
  ADC1_Configuration();
  TM_ILI9341_Init();

  Default_Screen();

  while(1){
    GPIO_Configuration();
    X1= ADCConvertedValues[0];
    Delay(8000);
    X2= ADCConvertedValues[2];

    // Read twice to avoid jumping points
    // Between both reading we add dealy

    GPIO_Configuration2();
    Y1= ADCConvertedValues[1];
    Y2= ADCConvertedValues[3];

    // Get average value to avoid jumping points
	  if(abs(X1-X2)<=2 && abs(Y1-Y2)<=2){
      X=(X1+X2)/2;
      Y=(Y1+Y2)/2;
    }

    Coordinate_Caculation(X,Y);

    if(x>=0 && x<=240 && y<=320 && y>=0) Print(x,y);

    /* The function below prints the adc reading coordinate********************/
    // printf("%d,%d,%d,%d\n",X1-10,X2-10,Y1-60,Y2-60);
    }
}
