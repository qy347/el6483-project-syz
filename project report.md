Project Report






##A short summary/abstract of your project (1 paragraph)

Our project is to develop a touchscreen application and build a drawing board. After six weeks’ work, we completed our goal and implemented the drawing board by Stm32f4 discovery board and 2.8 TFT LCD with touchscreen. We could detect the touch and read the X Y coordinates from the touch screen. And then display what we draw on the touchscreen. Meanwhile we added four color for choices and a screen clean touch button.



##A brief introduction, describing and motivating your project (3-6 paragraphs).


Today more and more devices are using touch screen technology, so we want to learn the princeples of the touch screen and how the touch screen works.That is why we decided to develop this touch screen application.

In order to reach our final goal of the project, we worked step by step. First, we studied the touchscreen princeples we use and link it with our discovery board. In this project, we use the 2.8 TFT LCD, which is a colorul touchscreen LCD, 240x320 resolution with individual RGB pixel controlability. This has way more resolution than a black and white 128x64 display. We use SPI to communicate with the LCD.

Second, we designed some codes to detect the touch. the LCD is a 4-wire resistive touch-screen, there will be a voltage change when we touch the screen. Therefore, we design a interrupt to detect the touch. when the touch comes, we will light the leds on the discovery board.

The next step was to read the X and Y coordinate, which is very important or the most difficult part of this design.The x and y coordinates of a touch on a 4-wire touch screen can be read in two steps. To read Y coordinates, Y+ is driven high, Y– is driven to ground, and the voltage at X+ is measured. We should leave the unesed X- pin unconfigured, otherwise the AD coverting will malfunction. The ratio of this measured voltage to the drive voltage applied is equal to the ratio of the y coordinate to the height of the touch screen. The x coordinate can be similarly obtained by driving X+ high, driving X– to ground, and measuring the voltage at Y+, with leaving the Y- unconfigured. The ratio of this measured voltage to the drive voltage applied is equal to the ratio of the x coordinate to the width of the touch screen. Finally, we can use the coordinates to draw a simple picture. In this project, we make use of the library functions which the LCD touchscreen provide for us. We can also choose colors when we draw something.
![](http://i.imgur.com/LtGrtQJ.png)

The library we use for displaying is here:
http://stm32f4-discovery.com/2014/05/library-11-button-library-for-ili9341-lcd-and-stmpe811-touch-controller-on-stm32f429-discovery-board/


##Hardware section


In this project, we bought a 2.8 TFT LCD with Touchscreen Breakout Board w/MicroSD Socket-ILI9341(http://www.adafruit.com/products/1770) from adafruit. The display can be used in two modes: 8-bit and SPI. For 8-bit mode, you'll need 8 digital data lines and 4 or 5 digital control lines to read and write to the display (12 lines total). SPI mode requires only 5 pins total (SPI data in, data out, clock, select, and d/c) but is slower than 8-bit mode. we do not care about the speed, so we chose the SPI mode. For the SPI mode, the pins and their functions are showed following:


Pins on screen | Functions
-------------- | -------------
3-5V Vin       | 3V pin
GND            | GND
CLK            | SPI clock
MISO           | SPI MISO
MOSI           | SPI MOSI
CS             | Chip Select
D/C            | SPI data/command select



we use SPI3 and PinPack2.
The connection diagram:

Pins on screen | Pins on board                |
-------------- | ---------------------------- |------------------------
3-5V Vin       | 3V                           |
GND            | GND                          |
CLK            | GPIOC                        |   10
MISO           | GPIOC                        |   11
MOSI           | GPIOC                        |   12
CS             | GPIOC                        |   2
D/C            | GPIOC                        |   13
RST            | GPIOC                        |   14
Y+             | GPIOA                        |   5
Y-             | GPIOB                        |   2
X+             | GPIOC                        |   0
X-             | GPIOD                        |   5



##software section

we use the STM32F4_Discovery_ILI9341 library (http://stm32f4-discovery.com/2014/04/library-08-ili9341-lcd-on-stm32f429-discovery-board/)
to light pixels, draw lines, cirlces and so on.
To read the X and Y coordinate, we developed the code by ourselves.


##instructions

Our touchscreen is easy to use. Link the LCD to the discovery board and run the code. We could use a pen to draw whatever you like on the screen. And we offer 4 different colors for choice, namely, yellow, blue, white, and red. The default color is white. You may notice there's a gray color block on the ritht bottom of the screen, that is the screen reset. Whenever touch is detected inside that block, it reset the screen to the default screen pattern. And the drawing will be erased. Also, in the bottom of the main function, we include a function to print the coordinates of every touch, you may use the openocd to monitor the changes of the coordinate.

##discussion section
In order to approach our goals ,we faced many problems, but luckily we overcame most of them.

At the beginning, we used the interrupt to detect whether the screen is touched. And then we do something during the interrupt service routine to decode the X Y coordinates. So, we wrote interrupt service and light on LEDs on board if the screed is touched. It worked,but it is not a good way to read continuous coordinates if we want to draw something. Each time we touch the screen and we could detect and only read X Y coordinates once, lighting on a single pixel on the screen. On the other hand, ISR should be as simple as possible, or it will affect the stability of the system.  In fact, after studying the structure of the touchscreen, we found that there was no need to design a interrupt to read the coordinates, we could do it directly by polling, so we cancelled the interrupt.

The most disturbing problem was to read the X and Y coordinates. Because we should change the configuration of the GPIO before reading the next direction of coordinate. we should store the datas we first read and then change the GPIO to read the coordinate in other direction. So it was a  difficult problem. First, We configure analog mode on X+ and Y+ which connected two ADCs. Then we should determine whether we use two ADCs or just One ADC with two channels. We found that using two ADCs was complex and difficult to configure. But if we use one ADC with two channels, we could only read coordinates correspondingly. After searching on Google, we found out that we could use DMA to store data from both channels. DMA is a effect way to transfer data without using the CPU, so we could linke the ADC to the DMA and get the rough coordinates data.

Another problem during reading the coordinate is the configuration of the GPIOs. We thought we could define any GPIOs as we like. But in fact, the side effect of the different GPIOs was significant. For example, during reading the Y coordinate, we first defined the X- as PC5, we detected a small range of Y. After changing it to PD5, we got the correct result. Besides, in our library, there was no floating input, so for reading X coordinate, we should leave Y- unconfigured and for reading Y coordinate, we should not configure the X-. Any configuration on those pins under the senario will lead to malfunction of reading. We should Deinit the corresponding GPIOs and this will influence how we manage our GPIO selection. Only in this way, can we read the right coordinates.


##result section

We developed several method to solve the accuracy problem in our project. The coordinate of the screen and the accual ADC reading value is not in a linear relation, further, the coordinates of both x and y are largely depending on each other. If we simply develop a linear transfomation between the ADC reading and the coordinates, there would be a huge deviation between the accual touch point and the pixels been light up.

![](http://i.imgur.com/1euyZ3j.jpg)
In the following picture, we first drew a straight yellow line in in the middle of the screen(by calling library function), then we manually drew a white line along the yellow one, to show the coordinates deviation when no compensation method has been taken. We can easily observe that the deviation on x direction is related to the position of both y and x. As x increases, the deviation becomes bigger. Also, the deviation becomes bigger and then smaller as y increases.

Thus, we figured a compensation function to modify the coordinate.
```
  if(Y>=0 && Y<=40) x=(X-10)*2.00+(70-(Y/2))*(0.5+(X/240));
  else if(Y>=150 && Y<=180) x=(X-10)*2.00+(80-(Y/2))*(0.6+(X/240));
  else x=(X-10)*2.00+(80-(Y/2))*(0.37+(X/240));
```
The compensation becomes bigger in two ends of the line, when it is smaller in the middle. In this way, we solve the coordinates deviation problem, and greatly enhanced the accuracy of the drawing board. The following picture shows the coordinates that has been compensated.
![](http://i.imgur.com/9BQzDTI.jpg)

This is the picture we draw.
![](http://i.imgur.com/szbo6Zu.jpg)



##conclusion section
Generally, we reached our goal, but there are still some defects. The reading of coordinates is not precise enough. Although we did some work, getting average value and adding offsets to eliminate this drift, deviation still exists between touch point and lighted pixels. We could make effort to further optimize this part.

For this project, everyone in our team learned a lot. We have a better understanding of the ADC section and GPIO configuration on STM32 discovery board and learned the use of DMA and 4 wire resistant touchscreen. More important, it is a good opportunity to use the knowledge we learnt in class.
